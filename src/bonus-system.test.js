import {calculateBonuses} from "./bonus-system.js";
const assert = require('assert')

const programs = [{name: 'Standard', multiplier: 0.05},
                  {name: 'Premium', multiplier: 0.1},
                  {name: 'Diamond', multiplier: 0.2},
                  {name: 'Random', multiplier: 0}]


const amounts = [{amount: 5000, bonus: 1},
                 {amount: 10000, bonus: 1.5},
                 {amount: 20000, bonus: 1.5},
                 {amount: 50000, bonus: 2},
                 {amount: 70000, bonus: 2},
                 {amount: 'Random', bonus: 2.5}]

describe('Bonus system tests', () => {
    test('Program: Standard, Amount: 5000', (done) => {
        var p = programs[0]
        var a = amounts[0]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });  
    test('Program: Standard, Amount: 10000', (done) => {
        var p = programs[0]
        var a = amounts[1]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Standard, Amount: 20000', (done) => {
        var p = programs[0]
        var a = amounts[2]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Standard, Amount: 50000', (done) => {
        var p = programs[0]
        var a = amounts[3]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Standard, Amount: 70000', (done) => {
        var p = programs[0]
        var a = amounts[4]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Premium, Amount: 5000', (done) => {
        var p = programs[1]
        var a = amounts[0]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });  
    test('Program: Premium, Amount: 10000', (done) => {
        var p = programs[1]
        var a = amounts[1]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Premium, Amount: 20000', (done) => {
        var p = programs[1]
        var a = amounts[2]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Premium, Amount: 50000', (done) => {
        var p = programs[1]
        var a = amounts[3]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Premium, Amount: 70000', (done) => {
        var p = programs[1]
        var a = amounts[4]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Diamond, Amount: 5000', (done) => {
        var p = programs[2]
        var a = amounts[0]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });  
    test('Program: Diamond, Amount: 10000', (done) => {
        var p = programs[2]
        var a = amounts[1]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Diamond, Amount: 20000', (done) => {
        var p = programs[2]
        var a = amounts[2]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Diamond, Amount: 50000', (done) => {
        var p = programs[2]
        var a = amounts[3]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Diamond, Amount: 70000', (done) => {
        var p = programs[2]
        var a = amounts[4]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Random, Amount: 5000', (done) => {
        var p = programs[3]
        var a = amounts[0]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });  
    test('Program: Random, Amount: 10000', (done) => {
        var p = programs[3]
        var a = amounts[1]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Random, Amount: 20000', (done) => {
        var p = programs[3]
        var a = amounts[2]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Random, Amount: 50000', (done) => {
        var p = programs[3]
        var a = amounts[3]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
    test('Program: Random, Amount: 70000', (done) => {
        var p = programs[3]
        var a = amounts[4]
        expect(calculateBonuses(p.name, a.amount)).toEqual(p.multiplier * a.bonus);
        done()
    });
  })
  